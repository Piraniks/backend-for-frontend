from django.db import models


class Author(models.Model):

    name = models.CharField(max_length=300)
    birth_date = models.DateField()
    homepage = models.URLField()
    description = models.TextField()

    def __str__(self):
        return self.name


class Genre(models.Model):

    name = models.CharField(max_length=300, unique=True)
    description = models.TextField()

    def __str__(self):
        return self.name


class Tag(models.Model):

    name = models.CharField(max_length=300, unique=True)

    def __str__(self):
        return self.name


class Book(models.Model):

    title = models.CharField(max_length=300)
    pages = models.IntegerField()
    description = models.TextField()
    isbn10 = models.CharField(max_length=11, verbose_name='ISBN-10', unique=True)
    isbn13 = models.CharField(max_length=14, verbose_name='ISBN-13', unique=True)

    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    genre = models.ForeignKey(Genre, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag, blank=True)

    prequel = models.ForeignKey(
        'self', on_delete=models.CASCADE,
        blank=True, null=True, related_name='+')
    sequel = models.ForeignKey(
        'self', on_delete=models.CASCADE,
        blank=True, null=True, related_name='+')

    def __str__(self):
        return self.title
