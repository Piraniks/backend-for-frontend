from django.contrib import admin

from .models import Author, Book, Genre, Tag


class AuthorAdmin(admin.ModelAdmin):
    list_display = ['name', 'birth_date', 'homepage']


class BookAdmin(admin.ModelAdmin):
    list_display = ['title', 'isbn10', 'isbn13', 'author', 'prequel', 'sequel']


class GenreAdmin(admin.ModelAdmin):
    list_display = ['name']


class TagAdmin(admin.ModelAdmin):
    list_display = ['name']


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Tag, TagAdmin)
