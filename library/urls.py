from django.urls import path
from .views import (
    AuthorView, BookView, GenreView, MainView,
    AuthorListView, BookListView, GenreListView,
)


urlpatterns = [
    path('', MainView.as_view()),
    path('author/', AuthorListView.as_view()),
    path('book/', BookListView.as_view()),
    path('genre/', GenreListView.as_view()),
    path('author/<int:id>/', AuthorView.as_view()),
    path('book/<int:id>/', BookView.as_view()),
    path('genre/<int:id>/', GenreView.as_view()),
]
