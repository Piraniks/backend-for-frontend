from django.shortcuts import render
from django.views import View
from rest_framework.viewsets import ModelViewSet
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer

from .models import Author, Book, Genre, Tag
from .serializers import (
    AuthorSerializer, BookSerializer,
    GenreSerializer, TagSerializer,)


class MainView(View):

    def get(self, request):
        return render(request, template_name='main_page.html',
                      context={
                          'title': 'Main Page',
                      })


class AuthorView(View):

    def get(self, request, id):
        author = Author.objects.filter(id=id).first()
        return render(request, template_name='author_page.html',
                      context={
                          'title': 'Author Page',
                          'author': author,
                      })


class BookView(View):

    def get(self, request, id):
        book = Book.objects.filter(id=id).first()
        return render(request, template_name='book_page.html',
                      context={
                          'title': 'Book Page',
                          'book': book,
                      })


class GenreView(View):

    def get(self, request, id):
        genre = Genre.objects.filter(id=id).first()
        return render(request, template_name='genre_page.html',
                      context={
                          'title': 'Genre Page',
                          'genre': genre,
                      })


class AuthorListView(View):

    def get(self, request):
        authors = Author.objects.all()
        return render(request, template_name='list_authors.html',
                      context={
                          'title': 'Author Page',
                          'authors': authors,
                      })


class BookListView(View):

    def get(self, request):
        books = Book.objects.all()
        return render(request, template_name='list_books.html',
                      context={
                          'title': 'Book Page',
                          'books': books,
                      })


class GenreListView(View):

    def get(self, request):
        genres = Genre.objects.all()
        return render(request, template_name='list_genres.html',
                      context={
                          'title': 'Genre Page',
                          'genres': genres,
                      })


# REST API

class AuthorViewSet(ModelViewSet):

    renderer_classes = (JSONRenderer,)
    queryset = Author.objects
    serializer_class = AuthorSerializer


class BookViewSet(ModelViewSet):

    renderer_classes = (JSONRenderer,)
    queryset = Book.objects
    serializer_class = BookSerializer


class GenreViewSet(ModelViewSet):

    renderer_classes = (JSONRenderer,)
    queryset = Genre.objects
    serializer_class = GenreSerializer


class TagViewSet(ModelViewSet):

    renderer_classes = (JSONRenderer,)
    queryset = Tag.objects
    serializer_class = TagSerializer


class BrowsableAuthorViewSet(AuthorViewSet):
    renderer_classes = (BrowsableAPIRenderer,)


class BrowsableBookViewSet(BookViewSet):
    renderer_classes = (BrowsableAPIRenderer,)


class BrowsableGenreViewSet(GenreViewSet):
    renderer_classes = (BrowsableAPIRenderer,)


class BrowsableTagViewSet(TagViewSet):
    renderer_classes = (BrowsableAPIRenderer,)
