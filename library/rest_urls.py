from rest_framework import routers

from .views import (
    AuthorViewSet, BrowsableAuthorViewSet,
    BookViewSet, BrowsableBookViewSet,
    GenreViewSet, BrowsableGenreViewSet,
    TagViewSet, BrowsableTagViewSet,)


router = routers.SimpleRouter()
router.register(r'author', AuthorViewSet)
router.register(r'book', BookViewSet)
router.register(r'genre', GenreViewSet)
router.register(r'tag', TagViewSet)
router.register(r'browsable/author', BrowsableAuthorViewSet)
router.register(r'browsable/book', BrowsableBookViewSet)
router.register(r'browsable/genre', BrowsableGenreViewSet)
router.register(r'browsable/tag', BrowsableTagViewSet)

urlpatterns = router.urls
