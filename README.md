# Learning for frontend
This project is meant to be used as a basic backend for developers learning how to use e.g. REST API.

##Download

Clone repository wherever you want it to be located
```
git clone https://gitlab.com/Piraniks/backend-for-frontend.git
```

Or go to [Project's Gitlab site](https://gitlab.com/Piraniks/backend-for-frontend.git) and download it from there.

## Run
### Create virtual environment with Python 3.6 (venv) or have Python 3.6 installed. 
If you don't use virtual environment, ignore this step and go to installing requirements.

Creating a virtualenv inside this project's directory.

```
python -m venv my_venv
cd my_venv
```

If you use Windows
```
Scripts\activate
```

If you use Linux
```
source bin/activate
```
### Install all needed packages.
```
pip install -r requirements.txt
```

### Apply migrations.
```
python manage.py migrate
```

### Create superuser

To create a superuser run the following command and fill in all needed data (e.g. username: admin, email: admin@admin.admin, password: adminpassword).
```
python manage.py createsuperuser
```
If you additionally want o fill in database with basic data, you can do if by running:
```
python manage.py loaddata default.json
```

### Run project.

| What | Where |
| ---- | --- |
| base site | localhost:8000 |
| admin site | localhost:8000/admin |
| rest api | localhost:8000/api |
| browsable rest api | localhost:8000/api/browsable |
```
python manage.py runserver
```

For more info about template engines and/or other configuration visit:
- [Django Project Homepage](https://www.djangoproject.com/ "Django's Homepage")

